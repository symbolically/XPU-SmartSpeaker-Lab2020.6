# make命令可以根据目标文件和依赖文件的修改时间，判断是否需要重新编译。
# $^ 代表所有依赖文件
# $@ 代表目标文件
tts: tts.c cJSON.c auth.c
	cc -Wall -D_GNU_SOURCE $^ -o $@ -lcurl

robot: robot.c cJSON.c
	cc -Wall $^ -o $@ -lcurl

stt: stt.c cJSON.c auth.c
	cc -Wall -D_GNU_SOURCE $^ -o $@ -lcurl
